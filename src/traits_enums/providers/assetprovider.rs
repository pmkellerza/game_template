use crate::prelude::*;
use std::fmt::{Debug, Formatter};
use std::sync::Arc;

pub trait AssetProvider {
    fn get_texture(&self, id: Arc<str>) -> &rl::Texture;
    fn get_sound(&self, id: Arc<str>) -> &rl::Sound;
    fn get_font(&self, id: Arc<str>) -> &rl::Font;
    fn get_music(&self, id: Arc<str>) -> &rl::Music;
    fn get_shader(&self, id: Arc<str>) -> &rl::Shader;
    fn get_asset_manager(&self) -> &AssetManager; //purely for impl Debug
}

impl Debug for dyn AssetProvider {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.get_asset_manager())
    }
}
