use crate::prelude::*;
use std::any::Any;

//This trait is used for concrete game states and is called by the state manager
pub trait GameSync {
    unsafe fn update(&mut self, context: &mut StateContext) -> StateTransition;
    unsafe fn draw(&mut self, context: &StateContext);
    unsafe fn as_any(&self) -> &dyn Any;
}
