use crate::traits_enums::gamesync::GameSync;

pub enum StateTransition {
    None,
    Push(Box<dyn GameSync>),
    Pop,
}
