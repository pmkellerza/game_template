use crate::prelude::{glm, rl};

//NA(GLM) conversion to Raylib Vector2
pub trait GLMRaymathConvert {
    fn into_vec2(&self) -> rl::Vector2;
}

impl GLMRaymathConvert for glm::Vec2 {
    fn into_vec2(&self) -> rl::Vector2 {
        rl::Vector2 {
            x: self.x,
            y: self.y,
        }
    }
}
