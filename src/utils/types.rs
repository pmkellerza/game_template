#![allow(unused_imports)]

use crate::prelude::*;
use hashbrown::HashMap;
use std::sync::Arc;

//AssetManager Types
pub type Textures = HashMap<Arc<str>, rl::Texture2D>;
pub type Sounds = HashMap<Arc<str>, rl::Sound>;
pub type Fonts = HashMap<Arc<str>, rl::Font>;
pub type Shaders = HashMap<Arc<str>, rl::Shader>;
pub type Music = HashMap<Arc<str>, rl::Music>;
