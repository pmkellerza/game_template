use crate::prelude::rl;
use raylib5_rs::rl_str;

#[derive(Debug)]
pub struct Track {
    pub name: String,
    pub artist: String,
    pub music: rl::Music,
}

impl Track {
    pub unsafe fn new(
        name: impl Into<String>,
        artist: impl Into<String>,
        file: impl Into<String>,
    ) -> Self {
        let name = name.into();
        let artist = artist.into();
        let file = file.into();

        let file = format!("assets/music/{}", file);
        let music = rl::LoadMusicStream(rl_str!(file));
        Self {
            name,
            artist,
            music,
        }
    }

    pub unsafe fn default(file: impl Into<String>) -> Self {
        let name = "".into();
        let artist = "".into();
        let file = file.into();

        let file = format!("assets/music/{}", file);
        let music = rl::LoadMusicStream(rl_str!(file));
        Self {
            name,
            artist,
            music,
        }
    }
}
