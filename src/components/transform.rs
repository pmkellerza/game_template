use crate::prelude::*;
#[derive(Debug)]
pub struct Transform2D {
    pub position: glm::Vec2,
    pub rotation: glm::Vec1,
    pub scale: glm::Vec1,
}

impl Transform2D {
    pub fn new(position: glm::Vec2) -> Self {
        Self {
            position,
            rotation: glm::Vec1::zeros(),
            scale: glm::Vec1::new(1.0),
        }
    }
}
