use crate::prelude::*;

mod components;
mod entities;
mod scripts;

pub struct PauseScreen {
    //button states
    gamescreen_state: bool,
    titlescreen_state: bool,
}

impl PauseScreen {
    pub unsafe fn new() -> Self {
        Self {
            titlescreen_state: false,
            gamescreen_state: false,
        }
    }
}

impl GameSync for PauseScreen {
    unsafe fn update(&mut self, context: &mut StateContext) -> StateTransition {
        let loader = context.get_assets_readlock();

        if self.titlescreen_state {
            return StateTransition::Push(Box::new(TitleScreen::new()));
        }

        if rl::IsKeyPressed(rl::KeyboardKey_KEY_ESCAPE as i32) {
            return StateTransition::Pop;
        }

        StateTransition::None
    }

    unsafe fn draw(&mut self, context: &StateContext) {
        let loader = context.get_assets_readlock();

        rl::ClearBackground(GameColor::RAYWHITE.into());

        let msg = "------ PAUSE MENU ------";
        rl::DrawText(
            rl_str!(msg),
            SCREEN_WIDTH / 2 - 150, //SCREEN_WIDTH / 2 - (msg.len() as i32) / 2,
            0,
            20,
            GameColor::DARKMAGENTA.into(),
        );

        //draw logo
        let logo = loader.get_texture("logo".into());
        let pos_x = (SCREEN_WIDTHF32 / 2.0) - (logo.width as f32 / 4.0);
        rl::DrawTextureEx(
            *logo,
            rl::Vector2 { x: pos_x, y: 40.0 },
            0.0,
            0.5,
            GameColor::WHITE.into(),
        );

        rl::ClearBackground(GameColor::RAYWHITE.into());
        let msg = "Press ESC to Leave Pause Menu";
        rl::DrawText(
            rl_str!(msg),
            SCREEN_WIDTH / 2 - 150, //SCREEN_WIDTH / 2 - (msg.len() as i32) / 2,
            SCREEN_HEIGHT / 2,
            20,
            GameColor::KELLYGREEN.into(),
        );

        if rl::GuiButton(
            rl::Rectangle {
                x: SCREEN_WIDTHF32 / 2.0 - (150.0 / 2.0),
                y: 700.0,
                width: 150.0,
                height: 50.0,
            },
            rl_str!("Exit to Title"),
        ) == 1
        {
            self.titlescreen_state = true;
        }
    }

    unsafe fn as_any(&self) -> &dyn Any {
        self
    }
}
