use crate::prelude::*;

mod components;
mod entities;
mod scripts;

pub struct GameScreen {
    //counter
    num: f32,

    //Button States
    pause_state: bool,
    titlescreen_state: bool,
}

impl GameScreen {
    pub unsafe fn new() -> Self {
        Self {
            num: 0.0,
            pause_state: false,
            titlescreen_state: false,
        }
    }
}

impl GameSync for GameScreen {
    unsafe fn update(&mut self, context: &mut StateContext) -> StateTransition {
        let _loader = context.get_assets_readlock();

        self.num += rl::GetFrameTime();

        if rl::IsKeyPressed(rl::KeyboardKey_KEY_ESCAPE as i32) {
            self.pause_state = true;
        }

        if self.titlescreen_state {
            self.titlescreen_state = false;
            return StateTransition::Push(Box::new(TitleScreen::new()));
        }

        if self.pause_state {
            self.pause_state = false;
            return StateTransition::Push(Box::new(PauseScreen::new()));
        }

        StateTransition::None
    }

    unsafe fn draw(&mut self, context: &StateContext) {
        let loader = context.get_assets_readlock();

        rl::ClearBackground(GameColor::RAYWHITE.into());
        let msg = "------ GAME STATE ------";
        rl::DrawText(
            rl_str!(msg),
            SCREEN_WIDTH / 2 - 150, //SCREEN_WIDTH / 2 - (msg.len() as i32) / 2,
            0,
            20,
            GameColor::KELLYGREEN.into(),
        );

        //draw logo
        let logo = loader.get_texture("logo".into());
        let pos_x = (SCREEN_WIDTHF32 / 2.0) - (logo.width as f32 / 4.0);
        rl::DrawTextureEx(
            *logo,
            rl::Vector2 { x: pos_x, y: 40.0 },
            0.0,
            0.5,
            GameColor::WHITE.into(),
        );

        //draw counter
        let font = loader.get_font("sonoma".into());
        let msg = format!("{:.2}", self.num);
        rl::DrawTextEx(
            *font,
            rl_str!(msg),
            rl::Vector2 {
                x: SCREEN_WIDTHF32 / 2.0 - 10.0,
                y: SCREEN_HEIGHTF32 / 2.5,
            },
            32.0,
            0.0,
            GameColor::ORANGEPEEL.into(),
        );

        //------RayGUI Buttons
        if rl::GuiButton(
            rl::Rectangle {
                x: SCREEN_WIDTHF32 / 2.0 - (150.0 / 2.0),
                y: 600.0,
                width: 150.0,
                height: 50.0,
            },
            rl_str!("Pause"),
        ) == 1
        {
            self.pause_state = true;
        };

        if rl::GuiButton(
            rl::Rectangle {
                x: SCREEN_WIDTHF32 / 2.0 - (150.0 / 2.0),
                y: 700.0,
                width: 150.0,
                height: 50.0,
            },
            rl_str!("Exit to Titlescreen"),
        ) == 1
        {
            self.titlescreen_state = true
        }
    }

    unsafe fn as_any(&self) -> &dyn Any {
        self
    }
}
