use crate::prelude::*;

mod components;
mod entities;
mod scripts;

pub struct TitleScreen {
    start_game: bool,
}

impl TitleScreen {
    pub unsafe fn new() -> Self {
        Self { start_game: false }
    }
}

impl GameSync for TitleScreen {
    unsafe fn update(&mut self, context: &mut StateContext) -> StateTransition {
        //handle input

        //if gui button is pressed/ or menu item is selected with keys
        if self.start_game {
            return StateTransition::Push(Box::new(GameScreen::new()));
        }

        StateTransition::None
    }

    unsafe fn draw(&mut self, context: &StateContext) {
        let loader = context.get_assets_readlock();

        rl::ClearBackground(GameColor::RAYWHITE.into());

        rl::DrawText(
            rl_str!("Congrats! You created your first window!"),
            190,
            200,
            20,
            GameColor::ICEBLUE.into(),
        );

        // Draw Logo

        let logo = loader.get_texture("logo".into());
        let pos_x = SCREEN_WIDTHF32 / 2.0 - (logo.width as f32 / 2.0);
        rl::DrawTextureV(
            *logo,
            rl::Vector2 { x: pos_x, y: 300.0 },
            GameColor::WHITE.into(),
        );

        //------RayGUI Buttons
        if rl::GuiButton(
            rl::Rectangle {
                x: SCREEN_WIDTHF32 / 2.0 - (150.0 / 2.0),
                y: 600.0,
                width: 150.0,
                height: 50.0,
            },
            rl_str!("Start Game"),
        ) == 1
        {
            self.start_game = true;
        };

        if rl::GuiButton(
            rl::Rectangle {
                x: SCREEN_WIDTHF32 / 2.0 - (150.0 / 2.0),
                y: 700.0,
                width: 150.0,
                height: 50.0,
            },
            rl_str!("Exit"),
        ) == 1
        {
            *CAN_QUIT.lock().unwrap() = true;
        }
    }
    unsafe fn as_any(&self) -> &dyn Any {
        self
    }
}
