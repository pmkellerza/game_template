#![allow(dead_code)]
#![allow(unused)]

use crate::prelude::*;
use std::sync::Mutex;

lazy_static! {
    //Quit Global bool
    pub static ref CAN_QUIT: Mutex<bool> = Mutex::new(false);
}

pub const SCREEN_WIDTH: i32 = 800;
pub const SCREEN_WIDTHF32: f32 = SCREEN_WIDTH as f32;
pub const SCREEN_HEIGHT: i32 = 900;
pub const SCREEN_HEIGHTF32: f32 = SCREEN_HEIGHT as f32;
pub const GAME_TITLE: &str = "--- Raylib 5 Game Template ---";
