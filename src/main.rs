#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

#[macro_use]
extern crate lazy_static;

use crate::gameapp::GameApp;
use crate::prelude::*;

mod components;
mod constants;
mod entities;
mod gameapp;
mod managers;
pub mod prelude;
mod states;
mod traits_enums;
mod utils;

fn main() {
    unsafe {
        let mut gameapp = GameApp::new(60);

        //game app loop
        while !*CAN_QUIT.lock().unwrap() {
            gameapp.update();

            gameapp.draw();

            *CAN_QUIT.lock().unwrap() = rl::WindowShouldClose() || *CAN_QUIT.lock().unwrap();
        }
    }
}
