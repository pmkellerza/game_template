pub mod container;
mod provider;

use crate::managers::utils::assetloader::*;
use crate::prelude::*;
use resunpacker::ResUnpacker;

#[derive(Debug)]
pub struct AssetManager {
    //Data Storage
    pub textures: Textures,
    pub sounds: Sounds,
    pub fonts: Fonts,
    pub music: Music,
    pub shaders: Shaders,

    //unpacker
    unpacker: ResUnpacker,
}

impl AssetManager {
    pub unsafe fn new() -> Self {
        Self {
            textures: Textures::new(),
            sounds: Sounds::new(),
            fonts: Fonts::new(),
            music: Music::new(),
            shaders: Shaders::new(),
            unpacker: ResUnpacker::new("./assets/config.ron".to_string()),
        }
    }

    pub unsafe fn load_assets(&mut self) {
        //Load Fonts
        load_fonts(&mut self.fonts, &self.unpacker);

        //Load Shaders
        load_shaders(&mut self.shaders, &self.unpacker);

        //Load Textures
        load_textures(&mut self.textures, &self.unpacker);

        //Load Music
        load_music(&mut self.music, &self.unpacker);

        //Load Sounds
        load_sounds(&mut self.sounds, &self.unpacker);
    }
}

impl Drop for AssetManager {
    fn drop(&mut self) {
        unsafe {
            //textures drop
            for (_, texture) in self.textures.drain() {
                rl::UnloadTexture(texture);
            }

            //Sound
            for (_, sound) in self.sounds.drain() {
                rl::UnloadSound(sound);
            }

            //Fonts
            for (_, font) in self.fonts.drain() {
                rl::UnloadFont(font);
            }

            //Music
            for (_, music) in self.music.drain() {
                rl::UnloadMusicStream(music);
            }

            //shaders
            for (_, shader) in self.shaders.drain() {
                rl::UnloadShader(shader);
            }

            println!("INFO: AssetManager Successfully Dropped ");
        }
    }
}
