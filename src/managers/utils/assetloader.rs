#![allow(unused)]

use crate::prelude::*;
use resunpacker::ResUnpacker;
use std::ffi::CString;
use std::ptr::{null, null_mut};

pub unsafe fn load_fonts(fonts: &mut Fonts, unpacker: &ResUnpacker) {
    unpacker.get_assets("fonts", |(id, asset)| {
        let font = rl::LoadFontFromMemory(
            rl_str!(".otf"),
            asset.as_ptr(),
            asset.len() as i32,
            32,
            null_mut(),
            0,
        );

        fonts.insert(id.into(), font);
    });
}

pub unsafe fn load_textures(textures: &mut Textures, unpacker: &ResUnpacker) {
    unpacker.get_assets("textures", |(id, asset)| {
        let image = rl::LoadImageFromMemory(rl_str!(".png"), asset.as_ptr(), asset.len() as i32);
        let texture = rl::LoadTextureFromImage(image);
        rl::UnloadImage(image);

        textures.insert(id.into(), texture);
    });
}
pub unsafe fn load_shaders(shaders: &mut Shaders, unpacker: &ResUnpacker) {
    unpacker.get_assets("shaders", |(id, asset)| {
        let asset = CString::from_vec_unchecked(asset).as_ptr();
        let shader = rl::LoadShaderFromMemory(null(), asset);

        shaders.insert(id.into(), shader);
    });
}
pub unsafe fn load_sounds(sounds: &mut Sounds, unpacker: &ResUnpacker) {
    unpacker.get_assets("sounds", |(id, asset)| {
        let wave = rl::LoadWaveFromMemory(rl_str!(".mp3"), asset.as_ptr(), asset.len() as i32);
        let sound = rl::LoadSoundFromWave(wave);
        rl::UnloadWave(wave);

        sounds.insert(id.into(), sound);
    });
}
pub unsafe fn load_music(music: &mut Music, unpacker: &ResUnpacker) {
    unpacker.get_assets("music", |(id, asset)| {
        let music_file =
            rl::LoadMusicStreamFromMemory(rl_str!(".mp3"), asset.as_ptr(), asset.len() as i32);

        music.insert(id.into(), music_file);
    })
}
