use crate::prelude::*;
use std::sync::Arc;

//AssetProvider provides functions to gives references/borrows of data in the AssetManager
//AssetProvider will than be wrapped around by Container that will be shared to game/application

impl AssetProvider for AssetManager {
    fn get_texture(&self, id: Arc<str>) -> &rl::Texture {
        self.textures.get(id.as_ref()).unwrap()
    }

    fn get_sound(&self, id: Arc<str>) -> &rl::Sound {
        self.sounds.get(id.as_ref()).unwrap()
    }

    fn get_font(&self, id: Arc<str>) -> &rl::Font {
        self.fonts.get(id.as_ref()).unwrap()
    }

    fn get_music(&self, id: Arc<str>) -> &rl::Music {
        self.music.get(id.as_ref()).unwrap()
    }

    fn get_shader(&self, id: Arc<str>) -> &rl::Shader {
        self.shaders.get(id.as_ref()).unwrap()
    }

    fn get_asset_manager(&self) -> &AssetManager {
        self
    }
}
