use crate::prelude::{AssetManager, AssetProvider};
use std::sync::{Arc, RwLock};

//The AssetContainer job is to give a Referencing counting pointer(atomic) of the AssetManager
// The Arc/Rc here is using RwLock as we tend to read more than write to AssetManager

//get_asset_provider() returns an Arc::Clone of ArcAssetProvider to anyone that wants to get assets from the AssetManager

pub type ArcAssetProvider = Arc<RwLock<dyn AssetProvider>>;

#[derive(Debug)]
pub struct AssetContainer {
    assets: ArcAssetProvider,
}

impl AssetContainer {
    pub unsafe fn new() -> Self {
        let mut assets = AssetManager::new();
        assets.load_assets();
        Self {
            assets: Arc::new(RwLock::new(assets)),
        }
    }

    pub unsafe fn get_asset_provider(&self) -> ArcAssetProvider {
        self.assets.clone() //Arc::clone()
    }
}
