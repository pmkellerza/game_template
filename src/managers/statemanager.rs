pub mod statecontext;

use crate::prelude::*;

pub struct StateManager {
    current_state: Option<Box<dyn GameSync>>,
    previous_state: Option<Box<dyn GameSync>>,
    context: StateContext,
}

impl StateManager {
    pub unsafe fn new(context: StateContext) -> Self {
        Self {
            context,
            current_state: None,
            previous_state: None,
        }
    }

    pub unsafe fn set_state<S: GameSync + 'static>(&mut self, state: S) {
        self.current_state = Some(Box::new(state))
    }

    pub unsafe fn update(&mut self) {
        if let Some(ref mut state) = self.current_state {
            let transition = state.update(&mut self.context);

            self.transition(transition)
        } else {
            panic!("No State was Set")
        }
    }

    pub unsafe fn draw(&mut self) {
        if let Some(ref mut state) = self.current_state {
            state.draw(&self.context)
        } else {
            panic!("No State Set")
        }
    }

    pub unsafe fn transition(&mut self, state_transition: StateTransition) {
        match state_transition {
            StateTransition::None => {}
            StateTransition::Push(new_state) => {
                //downcast new transition state to see if its the pausemenu inorder to store the current state to previous state
                if let Some(_) = new_state.as_any().downcast_ref::<PauseScreen>() {
                    // This means it's a pausescreen. we have to store current state to previous state
                    //take() takes the value out of the option leaving a None behind
                    self.previous_state = self.current_state.take();
                }

                self.current_state = Some(new_state);
            }
            StateTransition::Pop => {
                //Pop is used to get back to the previous stored state
                //This drops the current state for the previous state
                //previous_state than becomes None

                //only pausemenu is using Pop.
                //We have to move previous state into current state
                self.current_state = self.previous_state.take();
            }
        }
    }
}
