use crate::prelude::*;
use std::sync::{RwLockReadGuard, RwLockWriteGuard};

//Types alias for Locks
pub type AssetReadLock<'a> = RwLockReadGuard<'a, dyn AssetProvider + 'static>;
pub type AssetWriteLock<'a> = RwLockWriteGuard<'a, dyn AssetProvider + 'static>;

//StateContext holds/stores game resources like asset providers, references to other game data
pub struct StateContext {
    assets_provider: ArcAssetProvider,
}

//You can put methods to get assets and or game data from your StateContext
//Since assets_provider is a DI ArcAssetProvider = Arc<RwLock<dyn AssetProvider>> we don't put any methods.
//Methods are passed by the trait AssetProvider
impl StateContext {
    pub fn new(assets_provider: ArcAssetProvider) -> Self {
        Self { assets_provider }
    }

    pub fn get_assets_readlock(&self) -> AssetReadLock {
        self.assets_provider.read().unwrap()
    }

    pub fn get_assets_writelock(&self) -> AssetWriteLock {
        self.assets_provider.write().unwrap()
    }
}
