#![allow(unused_imports)]
#![cfg_attr(rustfmt, rustfmt_skip)]

//core
pub use std::any::Any;


// Raylib 5 FFI Imports
pub use raylib5_rs::ray_ffi as rl;
pub use raylib5_rs::rl_str;

//nalebra-glm
pub extern crate nalgebra_glm as glm;

//------- Game Internals --------------
//Global Utils
pub use crate::utils::{
    types::*,
    conversion::{nalgebra_convert::*,},
};

//Global Constants
pub use crate::constants::{colours::*, game_constants::*};

//Global Components
pub use crate::components::{
    track::*,
    transform::*,
};

//Global Entities
pub use crate::entities::{
    *,
};

//Managers
pub use crate::managers::{
    statemanager::{statecontext::*, StateManager},
    assetmanager::{container::*, AssetManager},
};

//States
pub use crate::states::{
    titlescreen::*,
    gamescreen::*,
    pausescreen::*,
};

//Global Traits and Enums
pub use crate::traits_enums::{
    providers::{assetprovider::*,},
    gamesync::*,
    statetransition::*,
};

