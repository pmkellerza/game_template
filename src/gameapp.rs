use crate::prelude::*;

pub struct GameApp {
    //Managers
    statemanager: StateManager,

    //DI AssetManager
    asset_container: AssetContainer,
}

impl GameApp {
    pub unsafe fn new(fps: i32) -> Self {
        rl::InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, rl_str!(GAME_TITLE));

        rl::SetTargetFPS(fps);

        rl::InitAudioDevice();

        rl::SetExitKey(0);

        //Create an AssetContainer. The Asset Container creates an AssetManager and
        // its wraps around the trait AssetProvider
        let asset_container = AssetContainer::new();

        //Get and AssetProvider and create a StateContext from it
        let asset_provider = asset_container.get_asset_provider();
        let state_context = StateContext::new(asset_provider);

        let mut statemanager = StateManager::new(state_context);

        //Set the inital state of the statemanager to titlescreen
        statemanager.set_state(TitleScreen::new());

        Self {
            asset_container,
            statemanager,
        }
    }

    pub unsafe fn update(&mut self) {
        //update call from state manager
        self.statemanager.update();
    }

    pub unsafe fn draw(&mut self) {
        rl::BeginDrawing();

        //Draw call from statemanager
        self.statemanager.draw();

        rl::EndDrawing();
    }
}

impl Drop for GameApp {
    fn drop(&mut self) {
        unsafe {
            //unload raylib data
            rl::CloseAudioDevice();

            rl::CloseWindow();
        }
    }
}
