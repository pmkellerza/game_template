use std::process::Command;

fn main() {
    //Target build directory
    let target_dir = get_cargo_target_dir().ok().unwrap();
    println!("cargo:warning=Target Dir: {:?}", &target_dir);

    //---------------------- Copy assets to target dir ------------------------------
    let cwd = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    println!("cargo:warning=CWD: {:?}", &cwd);
    let dest = format!("{}\\assets", target_dir.to_str().unwrap());
    let cmd_status = Command::new("xcopy")
        .current_dir(cwd)
        .args([
            ".\\assets\\*.*",
            dest.as_str(),
            "/E",
            "/H",
            "/C",
            "/I",
            "/Y",
        ])
        .status()
        .expect("");

    assert!(cmd_status.success());

    //------copy GNU dll to target dir ----------------------------------------
    let build_target = std::env::var("TARGET").unwrap();

    //based on your .cargo/config.toml settings
    let gnucompiler = "clang";

    if build_target.as_str() == "x86_64-pc-windows-gnu" {
        match gnucompiler {
            "clang" => clang_dll(target_dir),
            "gcc" => gcc_dll(target_dir),
            _ => unreachable!(),
        };
    }
}

fn get_cargo_target_dir() -> Result<std::path::PathBuf, Box<dyn std::error::Error>> {
    let out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR")?);
    let profile = std::env::var("PROFILE")?;
    let mut target_dir = None;
    let mut sub_path = out_dir.as_path();
    while let Some(parent) = sub_path.parent() {
        if parent.ends_with(&profile) {
            target_dir = Some(parent);
            break;
        }
        sub_path = parent;
    }
    let target_dir = target_dir.ok_or("not found")?;
    Ok(target_dir.to_path_buf())
}

fn gcc_dll(target_dir: std::path::PathBuf) {
    //libgcc_s_seh-1 dll
    let cwd = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    let dest = format!("{}", target_dir.to_str().unwrap());

    let cmd_status = Command::new("xcopy")
        .current_dir(cwd.clone())
        .args([
            "c:\\msys64\\mingw64\\bin\\libgcc_s_seh-1.dll",
            dest.as_str(),
            "/E",
            "/H",
            "/C",
            "/I",
            "/Y",
        ])
        .status()
        .expect("");

    assert!(cmd_status.success());

    //uncomment block below for libstdc++ dll if needed
    /*let cmd_status = Command::new("xcopy")
        .current_dir(cwd.clone())
        .args([
            "c:\\msys64\\mingw64\\bin\\libstdc++-6.dll",
            dest.as_str(),
            "/E",
            "/H",
            "/C",
            "/I",
            "/Y",
        ])
        .status()
        .expect("");

    assert!(cmd_status.success());*/
}

fn clang_dll(target_dir: std::path::PathBuf) {
    let cwd = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    let dest = format!("{}", target_dir.to_str().unwrap());
    let cmd_status = Command::new("xcopy")
        .current_dir(cwd)
        .args([
            "c:\\msys64\\clang64\\bin\\libc++.dll",
            dest.as_str(),
            "/E",
            "/H",
            "/C",
            "/I",
            "/Y",
        ])
        .status()
        .expect("");

    assert!(cmd_status.success());
}
